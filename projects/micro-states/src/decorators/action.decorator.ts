import { StateLogger } from '../logger/state-logger.service';
import { Indexable } from '../types/types';
import { getFunctionParamsNames } from '../utils/function-params';
import { StateService } from '../state/state.service';

export function Action() {
  return function (target: Indexable, key: string, descriptor: any) {
    const original = descriptor.value;
    const params = getFunctionParamsNames(target[key]);

    descriptor.value = function(...args: any[]) {
      const name = '[' + target.constructor.name + '] ' + key;
      const previousState = this.state;

      const result = original.apply(this, args);
      let type = '';
      //console.log(result);

      if (result) {
        type = Object.getPrototypeOf(result).constructor.name;
      }

      //console.log(type);

      const payload: Indexable = {};
      for (const paramIndex in params) {
        const index = Number.parseInt(paramIndex);
        const param = params[index];
        payload[param] = args[index];
      }

      if (type === 'Observable') {
        result.subscribe((result: any) => {
          StateLogger.asyncActionSuccess(name, result, this.state, previousState);
        });
      }

      StateLogger.action(name, payload, this.state, previousState);

      return result;
    }

    return descriptor;
  }
}
