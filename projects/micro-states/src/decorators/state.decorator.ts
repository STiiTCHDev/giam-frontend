import { Constructor, StateModel } from "../types/types";
import 'reflect-metadata';

export interface StateOptions<T> {
  name: string;
  initial: T;
}

export function State<State extends StateModel>(options: StateOptions<State>) {
  return <T extends Constructor>(target: T) => {
    const prototype = target.prototype;
    Reflect.defineMetadata('name', options.name, prototype);
    Reflect.defineMetadata('initial', options.initial, prototype);
    return target;
  }
}
