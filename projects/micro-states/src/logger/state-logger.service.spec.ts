/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StateLoggerService } from './state-logger.service';

describe('Service: StateLogger', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StateLoggerService]
    });
  });

  it('should ...', inject([StateLoggerService], (service: StateLoggerService) => {
    expect(service).toBeTruthy();
  }));
});
