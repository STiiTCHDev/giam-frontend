import { Inject, Injectable } from '@angular/core';
import { Indexable, StateModel } from '../types/types';
import { MICRO_STATES_CONFIG, MicroStatesModuleConfigs } from '../module.config';
import { Timer } from './timer';
import { ServiceLocator } from '../utils/service-locator';

@Injectable({
  providedIn: 'root'
})
export class StateLogger {

  private static timer: Timer;
  private static config: MicroStatesModuleConfigs.Logs;

  constructor() {
    StateLogger.config = ServiceLocator.injector.get(MICRO_STATES_CONFIG).logs;
    StateLogger.timer = new Timer();
  }

  public initialState(name: string, state: StateModel): void {
    if(StateLogger.config.active && StateLogger.config.initialState?.active) {
      const groupName = '%c[' + name + '] initialized (' + StateLogger.timer.time + ')';
      const logs = {
        'state': state,
      }
      StateLogger.logGroup(groupName, logs, StateLogger.config.initialState.color ?? '#74b9ff');
    }
  }

  public setState(name: string, setState: StateModel, previousState: StateModel): void {
    if (StateLogger.config.active && StateLogger.config.setState?.active) {
      const groupName = '%c' + name + ' (' + StateLogger.timer.time + ')';
      const logs = {
        'setState': setState,
        'previousState': previousState,
      }
      StateLogger.logGroup(groupName, logs, StateLogger.config.setState.color ?? 'white');
    }
  }

  public patchState(name: string, patchState: Partial<StateModel>, previousState: StateModel): void {
    if (StateLogger.config.active && StateLogger.config.patchState?.active) {
      const groupName = '%c' + name + ' (' + StateLogger.timer.time + ')';
      const logs = {
        'patchState': patchState,
        'previousState': previousState,
      }
      StateLogger.logGroup(groupName, logs, StateLogger.config.patchState.color ?? 'white');
    }
  }

  public static action(name: string, action: any, newState: StateModel, previousState: StateModel): void {
    if (StateLogger.config.active && StateLogger.config.actions?.active) {
      const groupName = '%c' + name + ' (' + StateLogger.timer.time + ')';
      const logs = {
        'action': action,
        'newState': newState,
        'previousState': previousState,
      }
      StateLogger.logGroup(groupName, logs, StateLogger.config.actions.color ?? '#2ed573');
    }
  }

  public static asyncActionSuccess(name: string, action: any, newState: StateModel, previousState: StateModel): void {
    if (StateLogger.config.active && StateLogger.config.asyncSuccess?.active) {
      const groupName = '%c' + name + ' success (' + StateLogger.timer.time + ')';
      const logs = {
        'action': action,
        'newState': newState,
        'previousState': previousState,
      }
      StateLogger.logGroup(groupName, logs, StateLogger.config.asyncSuccess.color ?? '#58B19F');
    }
  }

  private static logGroup(groupName: string, logs: Indexable, color: string = "white"): void {
    console.groupCollapsed(groupName, 'color:' + color + ';');
    for (let key in logs) {
      console.log(key, logs[key]);
    }
    console.groupEnd();
  }

}
