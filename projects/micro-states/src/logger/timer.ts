
export class Timer {

  private startDate: Date;
  private lastDate: Date;

  constructor() {
    this.start();
  }

  public get time(): string {
    const current = new Date();

    const sinceStart = this.getTimeSinceStart(current);
    let sinceLast = null;

    if (this.lastDate) {
      sinceLast = this.getTimeSinceLast(current);
      this.lastDate = current;
      return sinceStart + ', ' + sinceLast;
    }

    this.lastDate = current;
    return sinceStart;
  }

  private getTimeSinceStart(current: Date): string {
    const time = {min: 0, sec: 0, ms: 0};

    let diff = this.startDate.getTime() - current.getTime();
    if (diff > 0) {
      diff = 0;
    }
    diff = -diff;

    time.min = Math.floor(diff / 60000);
    diff = diff + (time.min * 60000);
    time.sec = Math.floor(diff / 1000);
    time.ms = diff % 1000;

    return '+' + time.min + ':' + time.sec + '.' + time.ms;
  }

  private getTimeSinceLast(current: Date): string {
    const time = {min: 0, sec: 0, ms: 0};

    let diff = this.lastDate.getTime() - current.getTime();
    if (diff > 0) {
      diff = 0;
    }
    diff = -diff;

    time.min = Math.floor(diff / 60000);
    diff = diff + (time.min * 60000);
    time.sec = Math.floor(diff / 1000);
    time.ms = diff % 1000;

    return '+' + time.min + ':' + time.sec + '.' + time.ms;
  }

  public start(): void {
    this.startDate = new Date();
  }

}
