import { NgModule, ModuleWithProviders, Injector } from '@angular/core';

import { MICRO_STATES_CONFIG, MicroStatesModuleConfigs } from './module.config';
import { StateService } from './state/state.service';
import { StateLogger } from './logger/state-logger.service';
import { ServiceLocator } from './utils/service-locator';


@NgModule()
export class MicroStatesModule {

  constructor(private injector: Injector) {
    ServiceLocator.injector = injector;
  }

  static forRoot(config?: MicroStatesModuleConfigs.Global): ModuleWithProviders<MicroStatesModule> {
    return {
      ngModule: MicroStatesModule,
      providers: [
        { provide: MICRO_STATES_CONFIG, useValue: config},
        StateService,
        StateLogger
      ]
    }
  }
}
