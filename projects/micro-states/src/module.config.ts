import { InjectionToken } from "@angular/core";

export namespace MicroStatesModuleConfigs {

  export interface LogConfig {
    active: boolean;
    color?: string;
  }

  export interface Logs {
    active: boolean;
    initialState?: LogConfig;
    setState?: LogConfig;
    patchState?: LogConfig;
    actions?: LogConfig;
    asyncSuccess?: LogConfig;
  }

  export interface Global {
    logs: Logs;
  }

}

export const MICRO_STATES_CONFIG = new InjectionToken<MicroStatesModuleConfigs.Global>('config');
