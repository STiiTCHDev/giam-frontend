import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { StateLogger } from '../logger/state-logger.service';
import { Selector } from '../types/types';
import { ServiceLocator } from '../utils/service-locator';
import 'reflect-metadata';

@Injectable()
export class StateService<State> {

  protected logger: StateLogger;
  protected name: string;
  protected _state$: BehaviorSubject<State>;
  protected initialState: State;

  constructor() {
    this.logger = ServiceLocator.injector.get(StateLogger);
    this.name = this.getMetadata('name');
    this.initialState = this.getMetadata('initial');

    this._state$ = new BehaviorSubject(this.initialState);
    this.logger.initialState(this.name, this.state);
  }

  /**
   * Get State metatada from class constructor
   */
  private getMetadata(key: string): any {
    return Reflect.getMetadata(key, this);
  }

  /**
   * Get State
   */

  get state$(): Observable<State> {
    return this._state$.asObservable();
  }

  get state(): State {
    return this._state$.getValue();
  }

  /**
   * Select from State
   */
  select<T = any>(selector: Selector<T>): Observable<T> {
    return this._state$.pipe(
      map(state => selector(state))
    );
  }

  selectSnapshot<T>(selector: Selector<T>): T {
    return selector(this.state);
  }

  /**
   * Update State
   */

  protected setState(nextState: State): void {
    const previousState = this.state;
    this._state$.next(nextState);
    this.logger.setState(this.name + '.setState', nextState, previousState);
  }

  protected patchState(partialState: Partial<State>): void {
    const currentState = this.state;

    this.setState({
      ...currentState,
      ...partialState
    });

    this.logger.patchState(this.name + '.patchState', partialState, currentState);
  }

}
