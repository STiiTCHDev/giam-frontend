
/**
 * Common
 */
export interface Indexable {
  [key: string]: any;
}

/**
 * State
 */
export interface StateModel {

}

/**
 * Decorators
 */
export type Constructor = new (...args: any[]) => {};

/**
 * Selectors
 */
export type Selector<T> = (state: StateModel | any) => T;
