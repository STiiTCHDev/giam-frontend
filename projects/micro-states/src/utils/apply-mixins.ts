export function applyMixins(derivedContructor: any, constructors: any[]) {
  for(let constructor of constructors) {
    const properties = Object.getOwnPropertyNames(constructor.prototype);
    for (let property of properties) {
      Object.defineProperty(
        derivedContructor.prototype,
        property,
        Object.getOwnPropertyDescriptor(constructor.prototype, property) || Object.create(null)
      )
    }
  }
}
