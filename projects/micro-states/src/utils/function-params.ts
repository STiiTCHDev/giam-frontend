
const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
const ARGUMENT_NAMES = /([^\s,]+)/g;

export function getFunctionParamsNames(fn: any): any[] {
  const fnString = fn.toString().replace(STRIP_COMMENTS, '');
  let result = fnString.slice(fnString.indexOf('(')+1, fnString.indexOf(')')).match(ARGUMENT_NAMES);

  if(result === null) {
    result = [];
  }

  return result;
}
