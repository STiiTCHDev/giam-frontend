import { Injector } from '@angular/core';

export class ServiceLocator {

  private static _injector: Injector;

  public static get injector(): Injector {
    return  this._injector;
  }

  public static set injector(injector: Injector) {
    this._injector = injector;
  }

}
