import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SummaryComponent } from './modules/summary/summary.component';
import { AuthComponent } from './modules/auth/auth.component';
import { AuthGuard } from './core/guards/auth.guard';
import { SummaryResolver } from './modules/summary/summary.resolver';

const routes: Routes = [
  {
    path: 'login',
    component: AuthComponent
  },
  {
    path: 'summary',
    component: SummaryComponent,
    canActivate: [AuthGuard],
    resolve: {summary: SummaryResolver}
  },
  {
    path: '',
    redirectTo: 'summary',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
