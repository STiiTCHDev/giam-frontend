/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import {AppComponent} from '@app/app.component';
import {createComponentFactory, Spectator} from '@ngneat/spectator';
import {StoreModule} from '@app/store/store.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>;
  const createComponent = createComponentFactory({
    component: AppComponent,
    imports: [StoreModule, HttpClientModule, RouterModule]
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  /*it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should display navbar, menu & page', () => {
    expect(spectator.query('app-navbar')).toBeTruthy();
    expect(spectator.query('app-menu')).toBeTruthy();
  });*/
});
