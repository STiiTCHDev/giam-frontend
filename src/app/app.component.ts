import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AppState } from '@shared/states/app/app.state';
import { AuthState } from '@shared/states/app/auth.state';
import { Observable } from 'rxjs';
import { RouterState } from './shared/states/app/router.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public state$: Observable<any> = this.appState.state$;
  public isLoading$: Observable<boolean> = this.appState.select(AppState.isLoading);

  constructor(
    private appState: AppState,
    private authState: AuthState,
    private routerState: RouterState,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {

  }

  ngAfterContentChecked(): void {
    this.cdRef.detectChanges();
  }

  public onHomeClick(): void {
    this.appState.toggleMenu();
  }

}
