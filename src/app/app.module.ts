import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeNgModule } from '@shared/modules/primeng.module';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { BaseFormComponent } from './shared/components/base-form/base-form.component';
import { MicroStatesModule } from '@micro-states';

import { SummaryModule } from './modules/summary/summary.module';
import { AdminModule } from './modules/admin/admin.module';
import { AuthModule } from './modules/auth/auth.module';
import { NavigationModule } from './modules/navigation/navigation.module';

import { environment } from '@env/environment';
import { provideFirebaseApp } from '@angular/fire/app';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { provideFirestore } from '@angular/fire/firestore';
import { GoogleLoginProvider, SocialLoginModule } from 'angularx-social-login';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RouterState } from '@shared/states/app/router.state';


@NgModule({
  declarations: [
    AppComponent,
    BaseFormComponent,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    CoreModule,
    PrimeNgModule,
    MicroStatesModule.forRoot({
      logs: {
        active: true,
        initialState: { active: true },
        setState: { active: false },
        patchState: { active: false },
        actions: { active: true },
        asyncSuccess: { active: true },
      }
    }),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    SocialLoginModule,

    AppRoutingModule,

    AuthModule,
    NavigationModule,
    AdminModule,
    SummaryModule,
  ],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: true,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider('498150969135-da9o02dmst39mdg5fi43210s3u2nsrfb.apps.googleusercontent.com')
        }
      ]
    }
  }, RouterState],
  bootstrap: [AppComponent]
})
export class AppModule {}
