import { RouteReuseStrategy, DetachedRouteHandle, ActivatedRouteSnapshot, ActivatedRoute, UrlSegment } from '@angular/router';

export class AppRouteReuseStrategy implements RouteReuseStrategy {

    private storedRoutes = new Map<string, DetachedRouteHandle>();

    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        return true;
    }

    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
      this.storedRoutes.set(this.getPath(route), handle);
    }

    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        return !!route.routeConfig && !!this.storedRoutes.get(this.getPath(route));
    }

    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        return this.storedRoutes.get(this.getPath(route))!;
    }

    shouldReuseRoute(future: ActivatedRouteSnapshot, current: ActivatedRouteSnapshot): boolean {
        return future.routeConfig === current.routeConfig;
    }

    private getPath(route: ActivatedRouteSnapshot): string {
      return route!.routeConfig!.path!;
    }

}
