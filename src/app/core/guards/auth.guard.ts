import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { AppState } from "@shared/states/app/app.state";
import { AuthState } from "@shared/states/app/auth.state";
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private appState: AppState,
    private authState: AuthState
  ) {}

  public canActivate(): Observable<boolean> {
    const isAuthenticated = this.authState.selectSnapshot(AuthState.isAuthenticated);

    if (!isAuthenticated) {
      return this.authState.me().pipe(
        map(() => {
          if (this.authState.selectSnapshot(AuthState.isAuthenticated)) {
            return true;
          }

          return false;
        })
      )
    }

    return of(true);
  }
}
