import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AdminRoutes } from './admin.routing';
import { CharactersComponent } from './components/characters/characters.component';
import { WeaponsComponent } from './components/weapons/weapons.component';
import { PrimeNgModule } from '../../shared/modules/primeng.module';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    PrimeNgModule,
    ReactiveFormsModule,
    AdminRoutes
  ],
  declarations: [
    CharactersComponent,
    WeaponsComponent,
  ],
  exports: [
    CharactersComponent,
    WeaponsComponent,
  ]
})
export class AdminModule { }
