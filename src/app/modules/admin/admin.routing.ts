import { Routes, RouterModule } from '@angular/router';
import { ArtifactsComponent } from './components/artifacts/artifacts.component';
import { CharactersComponent } from './components/characters/characters.component';
import { CharactersResolver } from './components/characters/characters.resolver';
import { MaterialsComponent } from './components/materials/materials.component';
import { WeaponsComponent } from './components/weapons/weapons.component';
import { AuthGuard } from '../../core/guards/auth.guard';
import { WeaponsResolver } from './components/weapons/weapons.resolver';

const routes: Routes = [
  {
    path: 'admin/characters', component: CharactersComponent,
    canActivate: [AuthGuard],
    resolve: { characters: CharactersResolver }
  },
  {
    path: 'admin/weapons', component: WeaponsComponent,
    canActivate: [AuthGuard],
    resolve: { weapons: WeaponsResolver }
  },
  {path: 'admin/artifacts', component: ArtifactsComponent},
  {path: 'admin/materials', component: MaterialsComponent},
];

export const AdminRoutes = RouterModule.forChild(routes);
