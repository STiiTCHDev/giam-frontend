import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { BaseFormComponent } from '@shared/components/base-form/base-form.component';
import { Character } from '@shared/types/entities/character';
import { Element } from '@shared/enums/elements.enum';
import { WeaponType } from '@shared/enums/weapon-type.enum';
import { FormGeneratorService } from "@shared/services/form-generator/form-generator.service";
import { CharactersState } from '@shared/states/database/characters.state';
import { element } from 'protractor';


@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent extends BaseFormComponent {

  public isUpdating: boolean = false;
  public form: FormGroup;
  collection$: Observable<Character[]> = this.charactersState.select(CharactersState.collection);

  rarityValues = ['4', '5'];
  elementValues = Object.keys(Element);
  weaponTypeValues = Object.keys(WeaponType);

  constructor(private charactersState: CharactersState, private formGenerator: FormGeneratorService) {
    super();
  }

  ngOnInit(): void {
    this.form = this.formGenerator.createForm(new Character());
  }

  public updateMode(character: Character): void {
    this.form.patchValue(character);
    this.isUpdating = true;
  }

  public selectElement(element: string): void {
    this.form.patchValue({
      element: element
    });
  }

  public isElement(element: string): boolean {
    return this.form.value.element === element;
  }

  public selectWeaponType(weaponType: string): void {
    this.form.patchValue({
      weaponType: weaponType
    });
  }

  public submit(): void {
    this.form.value.rarity = Number(this.form.value.rarity);

    if (!this.isUpdating) {
      this.charactersState.create(this.form.value)
      .subscribe(() => {
        this.form.reset();
      });
    } else {
      this.charactersState.update(this.form.value)
        .subscribe(() => {
          this.form.reset();
          this.isUpdating = false;
        });
    }
  }
}
