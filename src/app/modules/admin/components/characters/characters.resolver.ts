import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Character } from '@shared/types/entities/character';
import { CharactersState } from '@shared/states/database/characters.state';
import { map, tap } from 'rxjs/operators';
import { AppState } from '@shared/states/app/app.state';

@Injectable({
  providedIn: 'root'
})
export class CharactersResolver implements Resolve<Character[]> {

  constructor(private state: CharactersState, private appState: AppState) { }


  public resolve() {
    const loaded = this.state.selectSnapshot(CharactersState.isLoaded);

    if (loaded) {
      return this.state.state.collection;
    }

    return this.state.loadAll().pipe(
      map(() => this.state.state.collection),
      tap(() => this.appState.setLoading(false))
    );
  }
}
