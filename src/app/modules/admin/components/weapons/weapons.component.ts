import { Component } from '@angular/core';
import { WeaponType } from 'src/app/shared/enums/weapon-type.enum';
import { FormGroup,  FormControl } from '@angular/forms';
import { Weapon } from '@shared/types/entities/weapon';
import { WeaponsState } from '@shared/states/database/weapons.state';
import { Observable, of } from 'rxjs';
import { FormGeneratorService } from '@shared/services/form-generator/form-generator.service';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';

@Component({
  selector: 'app-weapons',
  templateUrl: './weapons.component.html',
  styleUrls: ['./weapons.component.scss']
})
export class WeaponsComponent {

  public isUpdating: boolean;
  public form: FormGroup;
  public collection$: Observable<Weapon[]> = this.weaponsState.select(WeaponsState.collection);

  public rarityValues = ['4', '5'];
  public weaponTypeValues = Object.keys(WeaponType);

  constructor(private weaponsState: WeaponsState, private formGenerator: FormGeneratorService) { }

  ngOnInit() {
    this.form = this.formGenerator.createForm(new Weapon());
  }

  public updateMode(weapon: Weapon): void {
    this.form.patchValue(weapon);
    this.isUpdating = true;
  }

  public selectWeaponType(type: string) {
    this.form.patchValue({
      type: type
    });
  }

  public submit(): void {
    console.log(this.form.value);
    this.form.value.rarity = Number(this.form.value.rarity);

    if (!this.isUpdating) {
      this.weaponsState.create(this.form.value)
        .subscribe(() => {
          this.form.reset();
        });
    } else {
      this.weaponsState.update(this.form.value)
        .subscribe(() => {
          this.form.reset();
          this.isUpdating = false;
        });
    }
  }
}
