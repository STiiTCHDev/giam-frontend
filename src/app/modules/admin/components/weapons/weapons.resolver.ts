import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Weapon } from '@shared/types/entities/weapon';
import { WeaponsState } from '../../../../shared/states/database/weapons.state';
import { map, tap } from 'rxjs/operators';
import { AppState } from '../../../../shared/states/app/app.state';


@Injectable({
  providedIn: 'root'
})
export class WeaponsResolver implements Resolve<Weapon[]> {

  constructor(private state: WeaponsState, private appState: AppState) { }

  public resolve() {
    const loaded = this.state.selectSnapshot(WeaponsState.isLoaded);

    if (loaded) {
      return this.state.state.collection;
    }

    return this.state.loadAll().pipe(
      map(() => this.state.state.collection),
      tap(() => this.appState.setLoading(false))
    )
  }

}
