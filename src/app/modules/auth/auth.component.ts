import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AuthState } from '../../shared/states/app/auth.state';
import { AppState } from '../../shared/states/app/app.state';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public form: FormGroup;

  constructor(
    public authState: AuthState,
    private appState: AppState
  ) { }

  ngOnInit() {
    //this.appState.setLoading(false);
  }

  ngAfterViewInit(): void {
      this.appState.setLoading(false);
  }

  onSubmit() {
    this.authState.loginWithGoogle();
  }
}
