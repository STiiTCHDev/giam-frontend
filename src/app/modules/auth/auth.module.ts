
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { FormComponentsModule } from "src/app/shared/modules/form-components/form-components.module";
import { AuthComponent } from "./auth.component";
import { PrimeNgModule } from '../../shared/modules/primeng.module';
import { AuthState } from "../../shared/states/app/auth.state";


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormComponentsModule,
    PrimeNgModule
  ],
  declarations: [
    AuthComponent,
  ],
  providers: [
    AuthState
  ]
})
export class AuthModule { }
