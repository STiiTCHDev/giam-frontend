import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  //@Select(AuthState.isAuthenticated) public isAuthenticated$: Observable<boolean>;
  public items: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.items = [
      {
        label:'Navigation',
        items: [
          {
            label: 'Résumé du compte',
            icon: 'fas fa-tachometer-alt',
            routerLink: 'summary'
          },
        ]
      },
      {
        label:'Administration',
        items: [
          {
            label: 'Personnages',
            icon: 'fas fa-users',
            routerLink: 'admin/characters'
          },
          {
            label: 'Armes',
            icon: 'fas fa-utensils',
            routerLink: 'admin/weapons'
          },
          {
            label: 'Artéfacts',
            icon: 'fas fa-tshirt',
            routerLink: 'admin/artifacts'
          },
          {
            label: 'Matériaux',
            icon: 'fas fa-boxes',
            routerLink: 'admin/materials'
          },
        ]
      },
    ];
  }

}
