import { Component, OnInit, Input } from '@angular/core';
import { User } from '@shared/types/entities/user';

@Component({
  selector: 'app-navbar-account',
  templateUrl: './navbar-account.component.html',
  styleUrls: ['./navbar-account.component.scss']
})
export class NavbarAccountComponent implements OnInit {

  @Input() user: User | null = {};
  @Input() isAuthenticated: boolean | null = false;

  constructor() { }

  ngOnInit() {

  }

}
