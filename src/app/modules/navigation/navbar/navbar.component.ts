import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { User } from '@shared/types/entities/user';
import { SelectSnapshot } from '@ngxs-labs/select-snapshot';
import { AuthState } from '@shared/states/app/auth.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output() home: EventEmitter<any> = new EventEmitter();

  public user: Observable<User> = this.authState.select(AuthState.user) as Observable<User>;
  public isAuthenticated: Observable<boolean> = this.authState.select(AuthState.isAuthenticated);

  constructor(private authState: AuthState) { }

  ngOnInit() {

  }

  onMenuClick() {
    this.home.emit(true);
  }

}
