import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';
import { PrimeNgModule } from '../../shared/modules/primeng.module';
import { NavbarAccountComponent } from './navbar/navbar-account/navbar-account.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PrimeNgModule
  ],
  declarations: [
    NavbarComponent,
    NavbarAccountComponent,
    MenuComponent,
  ],
  exports: [
    NavbarComponent,
    MenuComponent,
  ]
})
export class NavigationModule { }
