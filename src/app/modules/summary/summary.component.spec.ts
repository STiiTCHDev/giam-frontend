/* tslint:disable:no-unused-variable */
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator';
import {SummaryComponent} from '@modules/summary/summary.component';
import {NgxsModule, Store} from '@ngxs/store';
import {SummaryState} from '@app/store/app/pages/summary.state';
import {of} from 'rxjs';
import {mockSelector} from '@shared/testing/components/mockSelector';
import {Character} from '@shared/types/entities/character';

describe('SummaryComponent',
  () => {
    let spectator: Spectator<SummaryComponent>;
    const createComponent = createComponentFactory({
      component: SummaryComponent,
      detectChanges: false,
      mocks: [Store, SummaryState],
      imports: [NgxsModule.forRoot()]
    });

    let store: SpyObject<Store>;
    let summaryState: SpyObject<SummaryState>;

    beforeEach(() => {
      spectator = createComponent();

      store = spectator.inject(Store);
      summaryState = spectator.inject(SummaryState);

      mockSelector(spectator.component, 'characters$', of([]));
      mockSelector(spectator.component, 'userCharacters$', of([]));
      store.select.and.returnValue(of({}));

      spectator.detectChanges();
    });

    it('should create', () => {
      // Created
      expect(spectator.component).toBeTruthy();
      // Displays all page elements
      expect(spectator.query('div.page-header')).toBeTruthy();
      expect(spectator.query('p-toolbar')).toBeTruthy();
      expect(spectator.query('app-user-characters-list')).toBeTruthy();
    });

    it('should open modal when click on add button', () => {
      spectator.click('[data-add-btn]');
      expect(spectator.query('p-dialog')).toHaveClass('isVisible');
    });

    xit('should add new character row when click on add button in modal and close modal', () => {

    });

    it('should dispatch SummaryState.toggleColouredMode action when click on coloured mode button', () => {
      spectator.click('[data-coloured-btn]');
      expect(summaryState.toggleColouredMode).toHaveBeenCalled();
    });

    xit('should be in coloured mode if true in SummaryState.colouredMode', () => {
      expect()
    });

  });
