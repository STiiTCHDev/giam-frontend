import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Character } from '@shared/types/entities/character';
import { UserCharacter } from '@shared/types/entities/user-character';
import { UntilDestroy } from '@ngneat/until-destroy';
import { SummaryState } from './summary.state';
import { CharactersState } from '@shared/states/database/characters.state';
import { UserCharactersState } from '@shared/states/database/user-characters.state';


@UntilDestroy()
@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit{

  public selected: Character;
  public isModalOpened = false;
  public characters$: Observable<Character[]> = this.charactersState.select(CharactersState.collection);
  public userCharacters$: Observable<UserCharacter[]> = this.userCharacterState.select(UserCharactersState.collection);

  constructor(
    public summaryState: SummaryState,
    private charactersState: CharactersState,
    private userCharacterState: UserCharactersState,
  ) { }

  ngOnInit(): void {

  }

  public openModal(): void {
    this.isModalOpened = true;
  }

  public closeModal(): void {
    this.isModalOpened = false;
  }

  public hasCharacter(character: Character): boolean {
    const userCharacters = this.userCharacterState.state.collection;

    const index = userCharacters.findIndex(userCharacter => {
      return userCharacter.character.id === character.id;
    });

    if (index === -1) return false;
    return true;
  }

  public selectCharacter(character: Character): void {
    this.selected = character;
  }

  public addCharacter(character: Character): void {
    /*const user = this.store.selectSnapshot(AuthState.user);
    console.log('user:', user);
    console.log('character:', character);

    const userCharacter = new UserCharacter();
    if (user?.id && character?.id) {
      userCharacter.user = user['@id'];
      userCharacter.characterInfo = character['@id'];

      console.log('userCharacter:', userCharacter);
      this.store.dispatch(new CreateUserCharacter(userCharacter));
      this.closeModal();
    }*/
  }

  /*
  }*/

}
