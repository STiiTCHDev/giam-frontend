import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryComponent } from './summary.component';
import { ModalModule } from 'src/app/shared/modules/modal/modal.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrimeNgModule } from '@shared/modules/primeng.module';
import {UserCharactersListComponent} from "@modules/summary/user-characters-list/user-characters-list.component";
import { SummaryState } from './summary.state';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    ModalModule,
    ReactiveFormsModule,
    PrimeNgModule
  ],
  declarations: [
    SummaryComponent,
    UserCharactersListComponent
  ],
  exports: [
    SummaryComponent
  ],
  providers: [
    SummaryState
  ]
})
export class SummaryModule { }
