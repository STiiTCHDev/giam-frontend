import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { AppState } from '@shared/states/app/app.state';
import { combineLatest, forkJoin, of } from 'rxjs';
import { map, switchMap, mergeMap, tap, concatMap } from 'rxjs/operators';
import { CharactersState } from '../../shared/states/database/characters.state';
import { UserCharactersState } from '../../shared/states/database/user-characters.state';

@Injectable({
  providedIn: 'root'
})
export class SummaryResolver implements Resolve<any> {

  constructor(
    private appState: AppState,
    private charactersState: CharactersState,
    private userCharactersState: UserCharactersState
  ) { }

  public resolve() {
    const charactersLoaded = this.charactersState.selectSnapshot(CharactersState.isLoaded);
    const userCharactersLoaded = this.userCharactersState.selectSnapshot(UserCharactersState.isLoaded);

    if (charactersLoaded && userCharactersLoaded) {
      return {
        characters: this.charactersState.selectSnapshot(CharactersState.collection),
        userCharacters: this.userCharactersState.selectSnapshot(UserCharactersState.collection),
      }
    }

    return combineLatest([
      this.charactersState.loadAll(),
      this.userCharactersState.loadAll()
    ]).pipe(
      tap(() => {
          if (this.charactersState.selectSnapshot(CharactersState.isLoaded)
              && this.userCharactersState.selectSnapshot(UserCharactersState.isLoaded)
          ) {
            this.appState.setLoading(false);
          }
      })
    );
  }
}
