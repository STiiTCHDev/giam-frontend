/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Summary.stateService } from './summary.state.service';

describe('Service: Summary.state', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Summary.stateService]
    });
  });

  it('should ...', inject([Summary.stateService], (service: Summary.stateService) => {
    expect(service).toBeTruthy();
  }));
});
