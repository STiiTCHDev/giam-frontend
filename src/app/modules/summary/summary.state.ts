import { Injectable } from '@angular/core';
import { Action, Selector, State, StateService } from '@micro-states';


export interface SummaryStateModel {
  isColorMode: boolean;
}

@State<SummaryStateModel>({
  name: 'SummaryState',
  initial: {
    isColorMode: false,
  }
})
@Injectable({ providedIn: 'root' })
export class SummaryState extends StateService<SummaryStateModel> {

  public static get isColorMode(): Selector<boolean> {
    return (state: SummaryStateModel) => {
      return state.isColorMode;
    }
  }

  constructor() {
    super();
  }

  @Action()
  public toggleColorMode() {
    this.patchState({
      isColorMode: !this.state.isColorMode
    })
  }

}
