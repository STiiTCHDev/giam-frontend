import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCharactersListComponent } from './user-characters-list.component';
import {createComponentFactory, Spectator, SpyObject} from '@ngneat/spectator';
import {of} from 'rxjs';
import {UserCharactersState} from '@app/store/entities/user-character/user-character.state';
import {NgxsModule, Store} from '@ngxs/store';
import {SummaryState} from '@app/store/app/pages/summary.state';
import {UserCharacter} from '@shared/types/entities/user-character';
import { PrimeNgModule } from '@shared/modules/primeng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateUserCharacter } from '../../../store/entities/user-character/user-character.actions';

describe('UserCharactersListComponent', () => {
  let spectator: Spectator<UserCharactersListComponent>;
  const createComponent = createComponentFactory({
    component: UserCharactersListComponent,
    detectChanges: false,
    mocks: [Store, SummaryState, UserCharactersState],
    imports: [NgxsModule.forRoot(), PrimeNgModule, FormsModule, ReactiveFormsModule]
  });

  const userCharactersMock: UserCharacter[] = [
    {
      characterInfo: '/api/characters/1',
      user: '/api/users/1',
      level: '1',
      affinity: 1,
      constellations: 0,
      attackSkillLevel: 1,
      elementarySkillLevel: 1,
      burstSkillLevel: 1,
      id: 1
    },
    {
      '@id': '/api/user_characters/1',
      '@type': 'UserCharacter',
      characterInfo: '/api/characters/1',
      user: '/api/users/1',
      level: '1',
      affinity: 1,
      constellations: 0,
      attackSkillLevel: 1,
      elementarySkillLevel: 1,
      burstSkillLevel: 1,
      id: 1
    }
  ];

  let store: SpyObject<Store>;
  let summaryState: SpyObject<SummaryState>;

  beforeEach(() => {
    spectator = createComponent();
    store = spectator.inject(Store);
    summaryState = spectator.inject(SummaryState);

    spectator.setInput('userCharacters', of(userCharactersMock));
    store.selectSnapshot(SummaryState).and.returnValue({
      colouredMode: false,
    });

    spectator.detectChanges();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
    expect(spectator.query('p-table')).toBeTruthy();
  });

  it('should show one line for each userCharacters input', () => {
    expect(spectator.queryAll('tr.form-row')).toHaveLength(userCharactersMock.length);
  });

  it('should send update action when click on edit button', () => {
    const button = spectator.queryAll('p-button');

    if (button && button[0]) {
      spectator.click(button[0]);
    }

    const mock = userCharactersMock[0];
    mock.isEdited = false;

    expect(store.dispatch).toHaveBeenCalledWith(new UpdateUserCharacter(mock));
  });
});
