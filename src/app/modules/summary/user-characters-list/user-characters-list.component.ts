import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Character} from '@shared/types/entities/character';
import {UserCharacter} from '@shared/types/entities/user-character';
import {Observable} from 'rxjs';
import {FormGeneratorService} from '@shared/services/form-generator/form-generator.service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import { SummaryState } from '../summary.state';
import { CharactersState } from '@shared/states/database/characters.state';
import { UserCharactersState } from '@shared/states/database/user-characters.state';
import { AppState } from '../../../shared/states/app/app.state';

@UntilDestroy()
@Component({
  selector: 'app-user-characters-list',
  templateUrl: './user-characters-list.component.html',
  styleUrls: ['./user-characters-list.component.scss']
})
export class UserCharactersListComponent implements OnInit {

  @Input() userCharacters: UserCharacter[] | null;
  public forms: FormGroup[];

  constructor(
    private appState: AppState,
    private summaryState: SummaryState,
    private charactersState: CharactersState,
    private userCharactersState: UserCharactersState,
    private formGenerator: FormGeneratorService
  ) { }

  ngOnInit(): void {
    if (this.userCharacters) {
      this.forms = this.createForms(this.userCharacters);
    }
  }

  public updateCharacter(userCharacter: UserCharacter | any): void {
    delete userCharacter.isEdited;
    this.appState.setLoading(true);
    this.userCharactersState.update(userCharacter).subscribe(() => {
      this.appState.setLoading(false);
    });
  }

  public getCharacterById(id: string): Character {
    return this.charactersState.selectSnapshot(CharactersState.byId(id));
  }

  public getRowClass$(userCharacter: UserCharacter): Observable<string> {
    return new Observable((subscribe) => {
      if (userCharacter.character?.id) {
        const isColouredMode = this.summaryState.selectSnapshot(SummaryState.isColorMode);

        if (isColouredMode) {
          const character = this.getCharacterById(userCharacter.character.id);
          subscribe.next('row-color-' + character.element);
        } else {
          subscribe.next('');
        }
      } else {
        subscribe.next('');
      }
    });
  }

  private createForms(userCharacters: UserCharacter[]): FormGroup[] {
    const group: FormGroup[] = [];
    for (const userCharacter of userCharacters) {
      const subGroup = this.formGenerator.createForm(userCharacter);
      subGroup.addControl('isEdited', new FormControl(false));
      subGroup.valueChanges.subscribe((value) => {
        if (!value.isEdited) {
          subGroup.patchValue({isEdited: true});
        }
      });

      group.push(subGroup);
    }
    return group;
  }
}
