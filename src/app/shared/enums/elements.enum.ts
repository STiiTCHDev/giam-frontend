export enum Element {
  anemo =  'anemo',
  cryo  =  'cryo',
  dendro = 'dendro',
  electro = 'electro',
  geo = 'geo',
  hydro = 'hydro',
  pyro = 'pyro',
}
