export enum WeaponType {
  bow       = 'bow',
  sword     = 'sword',
  claymore  = 'claymore',
  polearm   = 'polearm',
  catalyst = 'catalyst'
}
