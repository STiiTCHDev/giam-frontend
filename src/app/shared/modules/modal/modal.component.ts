import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { element } from 'protractor';
import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() id: string;
  @Input() title: string;
  private element: any;
  isActive: boolean;

  constructor(private modalService: ModalService, private elem: ElementRef) {
    this.element = elem.nativeElement;
    this.isActive = false;
  }

  ngOnInit() {
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    document.body.appendChild(this.element);

    this.element.addEventListener('click', (elem: any) => {
      if (elem.target.className === 'modal') {
        this.close();
      }
    });

    this.modalService.add(this);
  }

  ngOnDestroy() {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  open() {
    this.isActive = true;
    document.body.classList.add('modal-open');
  }

  close() {
    this.isActive = false;
    document.body.classList.remove('modal-open');
  }

}
