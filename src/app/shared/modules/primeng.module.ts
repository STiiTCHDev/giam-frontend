import { NgModule } from '@angular/core';

import { TableModule } from 'primeng/table';
import { ImageModule } from 'primeng/image';
import { InputTextModule } from 'primeng/inputtext';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { ProgressBarModule } from 'primeng/progressbar';
import { PanelModule } from 'primeng/panel';
import { ToolbarModule } from 'primeng/toolbar';

const PrimeNgModules = [
  TableModule,
  ImageModule,
  InputTextModule,
  DialogModule,
  DropdownModule,
  ButtonModule,
  MenuModule,
  MenubarModule,
  ProgressBarModule,
  PanelModule,
  ToolbarModule
];

@NgModule({
  imports: [
    PrimeNgModules
  ],
  exports: [
    PrimeNgModules
  ]
})
export class PrimeNgModule { }
