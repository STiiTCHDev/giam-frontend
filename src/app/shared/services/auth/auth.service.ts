import { Injectable } from '@angular/core';
import { Observable, from, of } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { environment } from '@env/environment';

import { User } from '../../types/entities/user';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { UsersService } from '../database/users.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private socialAuthService: SocialAuthService, private usersService: UsersService) {}

  public loginWithGoogle(): Observable<SocialUser> {
    return from(this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID));
  }

  public me(): Observable<User | null> {
    return this.socialAuthService.authState.pipe(
      mergeMap(socialUser => {
        return this.usersService.findByEmail(socialUser?.email);
      })
    );
  }

  public logout() {
    this.socialAuthService.signOut();
  }

}
