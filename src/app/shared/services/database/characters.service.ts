import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

import { Character } from '../../types/entities/character';
import { FirebaseService } from '../firebase.service';
import { Firestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class CharactersService  extends FirebaseService<Character> {

  protected collectionName: string = 'characters';

  constructor(protected firestore: Firestore) {
    super(firestore);
    this.init();
  }

}
