import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { UserCharacter } from "@shared/types/entities/user-character";
import { FirebaseService } from "../firebase.service";
import { Firestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class UserCharactersService extends FirebaseService<UserCharacter> {

  protected collectionName: string = 'userCharacters';

  constructor(protected firestore: Firestore) {
    super(firestore);
    this.init();
  }

}
