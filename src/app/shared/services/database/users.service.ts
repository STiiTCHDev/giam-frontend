import { Injectable } from '@angular/core';
import { collection, collectionData, Firestore, CollectionReference, collectionSnapshots, doc, docData } from '@angular/fire/firestore';
import { from, Observable } from 'rxjs';
import { User } from '@shared/types/entities/user';
import { addDoc } from 'firebase/firestore';
import { tap, map, mergeMap } from 'rxjs/operators';
import { SocialAuthService } from 'angularx-social-login';
import { FirebaseService } from '../firebase.service';
import { GoogleSocialUser } from '@shared/types/social/google-social-user';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends FirebaseService<User> {

  protected collectionName: string = 'users';
  private user$: any;

  constructor(protected firestore: Firestore) {
    super(firestore);
    this.init();
  }

  public createFromGoogle(socialUser: GoogleSocialUser): Observable<User> {
    const user: User = {
      name: socialUser.name,
      email: socialUser.email,
      avatar: socialUser.photoUrl,
      roles: [],

      characters: [],
    };

    return this.add(user);
  }

  public findByEmail(email: string): Observable<User | null> {
    return this.findAll().pipe(
      map((users: User[]) => {
        return users.find((user: User) => {
          return user.email === email;
        }) as User;
      })
    );
  }

}
