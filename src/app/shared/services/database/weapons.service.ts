import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { Weapon } from '@shared/types/entities/weapon';
import { FirebaseService } from '../firebase.service';
import { Firestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class WeaponsService extends FirebaseService<Weapon> {

  protected collectionName: string = 'weapons';

  constructor(protected firestore: Firestore) {
    super(firestore);
    this.init();
  }

}
