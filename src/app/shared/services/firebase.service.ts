import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { addDoc, docData, collectionData, CollectionReference, Firestore, doc, collection, updateDoc, setDoc } from '@angular/fire/firestore';
import { take, map, mergeMap } from 'rxjs/operators';
import { FirebaseItem } from '../types/patterns/firebase';

@Injectable({
  providedIn: 'root'
})
export abstract class FirebaseService<T extends FirebaseItem> {

  protected abstract collectionName: string
  protected collectionRef: CollectionReference<T>;

  constructor(protected firestore: Firestore) {}

  protected init() {
    this.collectionRef = collection(this.firestore, this.collectionName) as CollectionReference<T>;
  }

  public find(id: string): Observable<T> {
    const docRef = doc(this.firestore, this.collectionName + '/${id}');
    return docData(docRef, {idField: 'id'}) as Observable<T>;
  }

  public findAll(): Observable<T[]> {
    return collectionData(this.collectionRef, {idField: 'id'}).pipe(take(1));
  }

  public add(document: T): Observable<T> {
    return from(addDoc(this.collectionRef, document)).pipe(
      mergeMap(docRef => docData(docRef, {idField: 'id'}))
    );
  }

  public update(document: T) {
    const docRef = doc(this.firestore, this.collectionName + '/' + document.id);
    return from(setDoc(docRef, document));
  }

  public delete(id: string) {

  }


}
