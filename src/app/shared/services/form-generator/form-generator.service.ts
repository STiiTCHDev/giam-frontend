import { Injectable } from '@angular/core';
import {Form, FormControl, FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class FormGeneratorService {

  constructor() { }

  public createForm(model: any): FormGroup {
    const group = new FormGroup({});

    for (const attr of Object.keys(model)) {
      group.addControl(attr, new FormControl(model[attr]));
    }

    return group;
  }
}
