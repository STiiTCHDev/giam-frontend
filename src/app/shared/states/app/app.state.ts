import { Injectable } from '@angular/core';
import { Action, Selector, State, StateService } from '@micro-states';

export interface AppStateModel {
  isLoading: boolean;
  isMenuCompact: boolean;
}

@State<AppStateModel>({
  name: 'AppState',
  initial: {
    isLoading: true,
    isMenuCompact: false
  }
})
@Injectable({providedIn: 'root'})
export class AppState extends StateService<AppStateModel> {

  public static get isLoading(): Selector<boolean> {
    return (state: AppStateModel) => {
      return state.isLoading;
    }
  }

  constructor() {
    super();

    if (window.innerWidth < 992) {
      this.patchState({
        isMenuCompact: true
      })
    }
  }

  @Action()
  public setLoading(isLoading: boolean): void {
    this.patchState({
      isLoading: isLoading
    });
  }

  @Action()
  public toggleMenu(): void {
    const state = this.state;
    this.patchState({
      isMenuCompact: !state.isMenuCompact
    })
  }

}
