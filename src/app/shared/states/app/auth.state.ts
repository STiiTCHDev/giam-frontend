import { Injectable } from '@angular/core';
import { Action, Selector, State, StateService } from '@micro-states';
import { User } from '@shared/types/entities/user';
import { GoogleSocialUser } from '@shared/types/social/google-social-user';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { take, tap } from 'rxjs/operators';
import { UsersService } from '@shared/services/database/users.service';

export interface AuthStateModel {
  user: User | GoogleSocialUser | null;
}

@State<AuthStateModel>({
  name: 'AuthState',
  initial: {
    user: null
  }
})
@Injectable({ providedIn: 'root' })
export class AuthState extends StateService<AuthStateModel> {

  public static get user(): Selector<User | GoogleSocialUser | null> {
    return (state: AuthStateModel) => {
      return state.user ?? null;
    }
  }

  public static get isAuthenticated(): Selector<boolean> {
    return (state: AuthStateModel) => {
      if (state.user && state.user.email) {
        return true;
      }
      return false;
    }
  }

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private router: Router
  ) {
    super();
  }

  @Action()
  public loginWithGoogle() {
    this.authService.loginWithGoogle().subscribe(socialUser => {
      this.usersService.createFromGoogle(socialUser).subscribe(() => {
        this.me().subscribe(() => {
          this.router.navigateByUrl('/summary');
        });
      });
    });
  }

  @Action()
  public logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

  @Action()
  public me() {
    return this.authService.me().pipe(
      take(1),
      tap((me) => {
        if (me?.email) {
          this.setUser(me);
        } else {
          this.router.navigateByUrl('/login');
        }
      })
    );
  }

  @Action()
  public setUser(user: User | GoogleSocialUser) {
    this.patchState({
      user: user
    });
  }

}
