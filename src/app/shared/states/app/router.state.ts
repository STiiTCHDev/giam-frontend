import { Injectable } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Action, Selector, State, StateService } from '@micro-states';

export interface RouterStateModel {
  currentUrl: string;
}

@State<RouterStateModel>({
  name: 'RouterState',
  initial: {
    currentUrl: ''
  }
})
@Injectable({ providedIn: 'root' })
export class RouterState extends StateService<RouterStateModel> {

  public static get currentUrl(): Selector<string> {
    return (state: RouterStateModel) => {
      return state.currentUrl;
    }
  }

  constructor(private router: Router) {
    super();

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.navigateStart(event.url);
      }

      if (event instanceof NavigationEnd) {
        this.navigateSuccess(event.url);
      }

      if (event instanceof NavigationCancel) {
        this.navigateCancel(event.url);
      }
    });
  }

  @Action()
  public navigateStart(targetUrl: string) {

  }

  @Action()
  public navigateSuccess(url: string) {
    this.patchState({
      currentUrl: url
    });
  }

  @Action()
  public navigateCancel(url: string) {

  }

  @Action()
  public navigateError(url: string) {

  }

}
