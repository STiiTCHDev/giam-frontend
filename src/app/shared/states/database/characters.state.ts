import { Injectable } from '@angular/core';
import { Action, Selector, State, StateService } from '@micro-states';
import { Character } from '@shared/types/entities/character';
import { CharactersService } from '@shared/services/database/characters.service';
import { tap, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

export interface CharactersStateModel {
  collection: Character[];
  loaded: boolean;
}

@State<CharactersStateModel>({
  name: 'CharactersState',
  initial: {
    collection: [],
    loaded: false
  }
})
@Injectable({ providedIn: 'root' })
export class CharactersState extends StateService<CharactersStateModel> {

  public static get collection(): Selector<Character[]> {
    return (state: CharactersStateModel) => {
      return state.collection;
    }
  }

  public static byId(id: string): Selector<Character> {
    return (state: CharactersStateModel) => {
      return state.collection.filter((character: Character) => character.id === id)[0];
    }
  }

  public static get isLoaded(): Selector<boolean> {
    return (state: CharactersStateModel) => {
      return state.loaded;
    }
  }

  constructor(private service: CharactersService) {
    super();
  }

  @Action()
  public loadAll(): Observable<Character[]> {
    return this.service.findAll().pipe(
      tap((collection: Character[]) => {
        this.patchState({
          collection: collection,
          loaded: true
        });
      })
    );
  }

  @Action()
  public create(character: Character): Observable<Character> {
    return this.service.add(character).pipe(
      tap(() => this.loadAll())
    );
  }

  @Action()
  public update(character: Character) {
    return this.service.update(character);
  }

}
