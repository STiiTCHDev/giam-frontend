import { Injectable } from '@angular/core';
import { Action, Selector, State, StateService } from '@micro-states';
import { UserCharactersService } from '@shared/services/database/user-character.service';
import { UserCharacter } from '@shared/types/entities/user-character';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface UserCharactersStateModel {
  collection: UserCharacter[];
  loaded: boolean;
}

@State<UserCharactersStateModel>({
  name: 'UserCharactersState',
  initial: {
    collection: [],
    loaded: false
  }
})
@Injectable({ providedIn: 'root' })
export class UserCharactersState extends StateService<UserCharactersStateModel> {

  public static get collection(): Selector<UserCharacter[]> {
    return (state: UserCharactersStateModel) => {
      return state.collection;
    }
  }

  public static get isLoaded(): Selector<boolean> {
    return (state: UserCharactersStateModel) => {
      return state.loaded;
    }
  }

  constructor(private service: UserCharactersService) {
    super();
  }

  @Action()
  public loadAll(): Observable<UserCharacter[]> {
    return this.service.findAll().pipe(
      tap((collection: UserCharacter[]) => {
        this.patchState({
          collection: collection,
          loaded: true
        });
      })
    );
  }

  @Action()
  public create(userCharacter: UserCharacter) {
    /*return this.service.create(userCharacter).pipe(
      tap((response: UserCharacter) => {
        let collection = [...this.state.collection];
        collection.push(userCharacter);

        this.patchState({
            collection: collection
        });
      })
    );*/
  }

  @Action()
  public update(userCharacter: UserCharacter) {
    return this.service.update(userCharacter).pipe(
      tap(() => {
        this.loadAll();
      })
    );
  }

}
