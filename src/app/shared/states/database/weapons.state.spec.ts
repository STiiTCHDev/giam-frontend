/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CharactersStateService } from './characters-state.service';

describe('Service: CharactersState', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CharactersStateService]
    });
  });

  it('should ...', inject([CharactersStateService], (service: CharactersStateService) => {
    expect(service).toBeTruthy();
  }));
});
