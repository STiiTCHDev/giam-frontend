import { Injectable } from '@angular/core';
import { Weapon } from '@shared/types/entities/weapon';
import { Action, Selector, State, StateService } from '@micro-states';
import { Observable } from 'rxjs';
import { WeaponsService } from '@shared/services/database/weapons.service';
import { tap } from 'rxjs/operators';

export interface WeaponsStateModel {
  collection: Weapon[],
  loaded: boolean,
}

@State({
  name: 'WeaponsState',
  initial: {
    collection: [],
    loaded: false
  }
})
@Injectable({ providedIn: 'root' })
export class WeaponsState extends StateService<WeaponsStateModel> {

  public static get collection(): Selector<Weapon[]> {
    return (state: WeaponsStateModel) => {
      return state.collection;
    }
  }

  public static get isLoaded(): Selector<boolean> {
    return (state: WeaponsStateModel) => {
      return state.loaded;
    }
  }

  constructor(private service: WeaponsService) {
    super();
  }

  @Action()
  public loadAll(): Observable<Weapon[]> {
    return this.service.findAll().pipe(
      tap((collection: Weapon[]) => {
        this.patchState({
          collection: collection,
          loaded: true
        });
      })
    );
  }

  @Action()
  public create(weapon: Weapon) {
    return this.service.add(weapon).pipe(
      tap(() => this.loadAll())
    );
  }

  @Action()
  public update(weapon: Weapon) {
    return this.service.update(weapon);
  }

}
