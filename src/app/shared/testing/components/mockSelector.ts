
export function mockSelector(component: any, property: string, value: any): void {
  Object.defineProperty(component, property, { writable: true });
  component[property] = value;
}
