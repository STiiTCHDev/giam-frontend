import { createServiceFactory, SpectatorServiceFactory, SpectatorServiceOptions } from '@ngneat/spectator';
import { StateClass } from '@ngxs/store/internals';
import { StoreTestingModule } from '@store/store.testing.module';

export function createStateFactory<T>(state: StateClass): SpectatorServiceFactory<T> {

  const options: SpectatorServiceOptions = {
    service: state,
    imports: [
      StoreTestingModule
    ]
  };

  return createServiceFactory(options);
}
