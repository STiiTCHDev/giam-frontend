import { GameObject } from "@shared/types/patterns/game-object";

export interface Character extends GameObject {
  weaponType?: string;
  element?: string;
}

export class Character extends GameObject {
  public weaponType?: string = '';
  public element?: string = '';
}
