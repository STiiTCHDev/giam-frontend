
import { Character } from '@shared/types/entities/character';
import { User } from '@shared/types/entities/user';
import { FirebaseItem, Reference } from '../patterns/firebase';
import { GameObject } from '@shared/types/patterns/game-object';

export interface UserCharacter extends GameObject {
  user: User | string;
  character: Character | Reference;

  level?: string;
  affinity?: number;
  constellations?: number;
  attackSkillLevel?: number;
  elementarySkillLevel?: number;
  burstSkillLevel?: number;
}


export class UserCharacter {
  public user: User | string;
  public character: Character | Reference;

  public level?: string = '1';
  public affinity?: number = 1;
  public constellations?: number = 0;
  public attackSkillLevel?: number = 1;
  public elementarySkillLevel?: number = 1;
  public burstSkillLevel?: number = 1;
}
