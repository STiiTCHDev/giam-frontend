import { Character } from '@shared/types/entities/character';
import { FirebaseItem, Reference } from '../patterns/firebase';

export interface User extends FirebaseItem {
  name?: string;
  email?: string;
  avatar?: string;
  roles?: any[];

  characters?: Reference[];
}
