import { GameObject } from "@shared/types/patterns/game-object";

export interface Weapon extends GameObject {
  type: string;
}

export class Weapon extends GameObject {
  public type: string = '';
}
