
export interface FirebaseItem {
  id?: string;
}

export type Reference = FirebaseItem;
