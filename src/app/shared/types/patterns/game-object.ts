import { FirebaseItem } from '@shared/types/patterns/firebase';

export interface GameObject extends FirebaseItem {
  imgUrl?: string;
  name?: string;
  rarity?: number;
}

export class GameObject {
  public name?: string = '';
  public rarity?: number = 0;
}
