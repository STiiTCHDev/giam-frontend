export interface GoogleSocialUser {
  id: string;
  idToken: string;
  authToken: string;
  email: string;
  firstName: string;
  lastName?: string;
  name: string;
  photoUrl: string;
  provider: string;
  response: {
    idpId: string;
    id_token: string;
    access_token: string;
    token_type: string;
    login_hint: string;
    scope: string;
    first_issued_at: number;
    expires_at: number;
    expires_in: number;
  }
}
