export const environment = {
  production: true,
  apiBaseUrl: 'http://localhost:8000/api',
  firebase: {
    apiKey: "AIzaSyB4OOiSmVu3tjhmTSt6wHU1Hx6m0Bq6Aeg",
    authDomain: "genshin-assistant.firebaseapp.com",
    projectId: "genshin-assistant",
    storageBucket: "genshin-assistant.appspot.com",
    messagingSenderId: "498150969135",
    appId: "1:498150969135:web:0a91a22e378d2cb5f21bda",
    measurementId: "G-T8X1CKPVN7"
  }
};
