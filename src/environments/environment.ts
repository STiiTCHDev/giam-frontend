// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiBaseUrl: 'http://localhost:8000/api',
  firebase: {
    apiKey: "AIzaSyB4OOiSmVu3tjhmTSt6wHU1Hx6m0Bq6Aeg",
    authDomain: "genshin-assistant.firebaseapp.com",
    projectId: "genshin-assistant",
    storageBucket: "genshin-assistant.appspot.com",
    messagingSenderId: "498150969135",
    appId: "1:498150969135:web:0a91a22e378d2cb5f21bda",
    measurementId: "G-T8X1CKPVN7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
